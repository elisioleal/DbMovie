package com.elisioleal.dbmovie.services

import com.elisioleal.dbmovie.models.MovieResponse
import retrofit2.Call
import retrofit2.http.GET

interface MovieApilnterface {

    @GET("/3/movie/popular?api_key=fcb6188dc381c27658a694c99c2bbd12")
    fun getMovieList(): Call<MovieResponse>
}